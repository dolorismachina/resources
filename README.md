# Programming resources
## Coding challenges
- [UVA Online Judge](https://uva.onlinejudge.org/index.php?option=com_frontpage&Itemid=1)
- [CodingBat](http://codingbat.com/)
- [CodeChef](https://www.codechef.com/)
- [CodeWars[http://codewars.com]

## Tutorials
- [Tweening functions](http://upshots.org/actionscript/jsas-understanding-easing)
- [More on tweening by Robert Penner - inventor of the tweening equations](http://robertpenner.com/easing/)
- [Interpolation in Unity 3D using Lerp](https://chicounity3d.wordpress.com/2014/05/23/how-to-lerp-like-a-pro/)

## Game Art
### Paid
- [GamePartners](http://gameartpartners.com/)

## Game Tutorials
- [XNA Game Programming Adventures](http://xnagpa.net/index.php)
- [RB Whitaker's XNA, MonoGame and C# tutorials](http://rbwhitaker.wikidot.com/)
